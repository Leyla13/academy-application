package com.academy.AcademyApp.controller;

import com.academy.AcademyApp.dto.CourseDto;
import com.academy.AcademyApp.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController {
    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }
    @PostMapping("/addCourse")
    public ResponseEntity<String> addCourse (@RequestBody CourseDto course){
        String addCourse = courseService.createCourse(course);
        return  ResponseEntity.ok(addCourse);
    }

    @GetMapping("/Courses")
    public ResponseEntity<List<CourseDto>> getCourses (){
       List<CourseDto> courses = courseService.getCourses();
       return ResponseEntity.ok(courses);
    }

    @GetMapping("/course/{id}")
    public ResponseEntity<CourseDto> getCourse(@PathVariable("id") Long id){
        CourseDto course = courseService.getCourse(id);
        return ResponseEntity.ok(course);
    }

    @PutMapping("/editCourse/{id}")
    public  ResponseEntity<CourseDto> updateCourse(@PathVariable("id") Long id,@RequestBody CourseDto course){
        CourseDto updateCourse = courseService.updateCourse(id,course);
        return ResponseEntity.ok(updateCourse);
    }

    @DeleteMapping("removeCourse/{id}")
    public ResponseEntity<Boolean> deleteCourse(@PathVariable("id)")Long id){
        Boolean status = courseService.deleteCourse(id);
        return ResponseEntity.ok(status);
    }

    @GetMapping("/findCourseByTeacherName")
    public List<CourseDto> findCourseByTeacherName(@RequestParam String name){
        return courseService.findCourseByTeacherName(name);
    }


}
