package com.academy.AcademyApp.controller;

import com.academy.AcademyApp.dto.StudentDto;
import com.academy.AcademyApp.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/addStudent")
    public ResponseEntity<String> createStudent (@RequestBody StudentDto student){
        String addStudent = studentService.createStudent(student);
        return  ResponseEntity.ok(addStudent);
    }

    @GetMapping("/allStudents")
    public ResponseEntity<List<StudentDto>> getStudents(){
       List<StudentDto> students = studentService.getStudents();
       return ResponseEntity.ok(students);
    }

    @GetMapping(value = "/student/{id}")
    public ResponseEntity<StudentDto> getStudent(@PathVariable("id") Long id){
       StudentDto student = studentService.getStudent(id);
       return ResponseEntity.ok(student);

    }
    @PutMapping("/updateStudent/{id}")
    public ResponseEntity<StudentDto> updateStudent(
           @PathVariable("id") Long id,@RequestBody StudentDto student) {
        StudentDto updatedStudent = studentService.updateStudent(id, student);
        return ResponseEntity.ok(updatedStudent);
    }

   @DeleteMapping("/deleteStudent/{id}")
    public ResponseEntity<Boolean> deleteStudent(@PathVariable("id") Long id){
        Boolean status = studentService.deleteStudent(id);
        return ResponseEntity.ok(status);

    }

}
