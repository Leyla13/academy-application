package com.academy.AcademyApp.controller;

import com.academy.AcademyApp.dto.TeacherDto;
import com.academy.AcademyApp.service.TeacherService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teachers")
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping("/addTeacher")
    public ResponseEntity<TeacherDto> addTeacher(@RequestBody TeacherDto teacher){
        TeacherDto addTeacher = teacherService.addTeacher(teacher);
        return ResponseEntity.ok(addTeacher);
    }

    @GetMapping("/teachers")
    public  ResponseEntity<List<TeacherDto>> getTeachers(){
       List<TeacherDto> teachers = teacherService.getTeachers();
       return ResponseEntity.ok(teachers);
    }

    @GetMapping("/teacher/{id}")
    public ResponseEntity<TeacherDto> getTeacher(@PathVariable("id") Long id){
        TeacherDto teacher = teacherService.getTeacher(id);
        return ResponseEntity.ok(teacher);
    }
    @PutMapping("/editTeacher/id")
    public ResponseEntity<TeacherDto> updateTeacher(@PathVariable("id") Long id, TeacherDto teacher){
        TeacherDto updatedTeacher = teacherService.updateTeacher(id, teacher);
        return ResponseEntity.ok(updatedTeacher);

    }
    @DeleteMapping("deleteTeacher/{id}")
    public ResponseEntity<Boolean> deleteTeacher(@PathVariable("id")Long id){
        Boolean status = teacherService.deleteTeacher(id);
        return ResponseEntity.ok(status);

    }


}
