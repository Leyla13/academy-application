package com.academy.AcademyApp.controller;

import com.academy.AcademyApp.dto.AcademyDto;
import com.academy.AcademyApp.service.AcademyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/academy")
public class AcademyController {
    private final AcademyService academyService;

    public AcademyController(AcademyService academyService) {
        this.academyService = academyService;
    }

    @PostMapping("/addBranch")
    public ResponseEntity<AcademyDto> addAcademy(@RequestBody AcademyDto academy){
        AcademyDto addAcademy = academyService.addAcademy(academy);
        return ResponseEntity.ok(addAcademy);
    }

    @GetMapping("/branches")
    public ResponseEntity<List<AcademyDto>> getAcademies(){
        List<AcademyDto> academies = academyService.getAcademies();
        return ResponseEntity.ok(academies);
    }

    @GetMapping("/branch/{id}")
    public ResponseEntity<AcademyDto> getAcademy(@PathVariable("id") Long id){
       AcademyDto academy = academyService.getAcademy(id);
       return ResponseEntity.ok(academy);
    }

    @PutMapping("/editBranch/{id}")
    public ResponseEntity<AcademyDto> updateAcademy(@PathVariable("id")Long id, AcademyDto academy){
        AcademyDto updatedAcademy = academyService.updateAcademy(id,academy);
        return ResponseEntity.ok(updatedAcademy);
    }

    @DeleteMapping("/removeBranch/{id}")
    public ResponseEntity<Boolean> deleteAcademy(@PathVariable("id") Long id){
       Boolean status = academyService.deleteAcademy(id);
       return ResponseEntity.ok(status);

    }

}
