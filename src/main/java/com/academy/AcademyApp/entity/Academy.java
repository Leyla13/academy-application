package com.academy.AcademyApp.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Entity
public class Academy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     Long id;

    @Column(name = "branch_name")
     String name;

    @Column(name = "branch_address")
     String address;

//    @ToString.Exclude
    @JsonBackReference
    @OneToMany(mappedBy = "academy")
     List<Course> courseList;
}
