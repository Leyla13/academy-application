package com.academy.AcademyApp.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "students",
        uniqueConstraints = {
        @UniqueConstraint(name = "student_email_unique",columnNames = "student_email")
        }
)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "student_name",
            columnDefinition = "TEXT"
    )
    String firstName;

    @Column(name = "student_surname",
            columnDefinition = "TEXT")
    String lastName;

    @Column(name = "student_email")
    String email;



    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "enrolled_students",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    List<Course> courseList;

}
