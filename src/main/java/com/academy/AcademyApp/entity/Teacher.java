package com.academy.AcademyApp.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Entity
@Table(name = "teachers",
        uniqueConstraints = {
        @UniqueConstraint(name = "teacher_email_unique",columnNames = "teacher_email")
        }
)
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     Long id;

    @Column(
            name = "teacher_name",
            columnDefinition = "TEXT"
    )
     String firstName;

    @Column(
            name = "teacher_surname",
            columnDefinition = "TEXT"

    )
     String lastName;

    @Column(
            name = "teacher_email"
    )
     String email;



    @JsonBackReference
    @OneToMany(mappedBy = "teacher")
     List<Course> courseList;
}
