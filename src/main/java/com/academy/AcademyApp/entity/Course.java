package com.academy.AcademyApp.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "course_name")
    private String name;

    @Column(name = "start_date")
    private LocalDate start_date;

    @Column(name = "end_date")
    private LocalDate end_date;


    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;


    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "courseList")
    private List<Student> students;


    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "academy_id")
    private Academy academy;
}
