package com.academy.AcademyApp.repository;

import com.academy.AcademyApp.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    @Query("select c from Course c join fetch c.teacher t where t.firstName=:name")
    List<Course> findCourseByTeacherName (@Param("name") String name);

}
