package com.academy.AcademyApp.dto;

import lombok.Data;

import java.util.List;

@Data
public class AcademyDto {
    private String name;
    private String address;
    private List<Long> courseIdList;
}
