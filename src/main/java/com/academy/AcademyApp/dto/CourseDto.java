package com.academy.AcademyApp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CourseDto {
    private String name;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate start_date;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate end_date;
    private Long teacher_id;
//    private List<StudentDto> studentDtoList;
    private Long academy_id;


}

