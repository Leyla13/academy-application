package com.academy.AcademyApp.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StudentDto implements Serializable {

    private static final long serialVersionUID = -704573463992083983L;
    private String firstName;
    private String lastName;
    private String email;
    private List<Long> courseIdList;

}
