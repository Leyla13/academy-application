package com.academy.AcademyApp.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeacherDto implements Serializable {
    private static final Long SerialVersionUid = 1L;
    private String firstName;
    private String lastName;
    private String email;
    private List<Long> courseIdList;
}
