package com.academy.AcademyApp.service;

import com.academy.AcademyApp.dto.CourseDto;
import com.academy.AcademyApp.dto.StudentDto;
import com.academy.AcademyApp.entity.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {
    String createCourse(CourseDto course);
    List<CourseDto> getCourses();
    CourseDto getCourse(Long id);
    CourseDto updateCourse(Long id, CourseDto course);
    Boolean deleteCourse(Long id);
    List<CourseDto> findCourseByTeacherName(String name);

}
