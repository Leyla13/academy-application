package com.academy.AcademyApp.service;

import com.academy.AcademyApp.dto.AcademyDto;
import com.academy.AcademyApp.entity.Academy;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AcademyService {
    AcademyDto addAcademy(AcademyDto academy);
    List<AcademyDto> getAcademies();
    AcademyDto getAcademy(Long id);
    AcademyDto updateAcademy(Long id, AcademyDto academy);
    Boolean deleteAcademy(Long id);
}
