package com.academy.AcademyApp.service.implementations;

import com.academy.AcademyApp.dto.CourseDto;
import com.academy.AcademyApp.entity.Academy;
import com.academy.AcademyApp.entity.Course;
import com.academy.AcademyApp.entity.Teacher;
import com.academy.AcademyApp.repository.AcademyRepository;
import com.academy.AcademyApp.repository.CourseRepository;
import com.academy.AcademyApp.repository.TeacherRepository;
import com.academy.AcademyApp.service.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class CourseServiceImp implements CourseService {
    private final CourseRepository courseRepository;
    private final AcademyRepository academyRepository;
    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;

    @Override
    public String createCourse(CourseDto courseDto) {
        Course course = modelMapper.map(courseDto,Course.class);
        courseRepository.save(course);

        return "course saved";
    }

    @Override
    public List<CourseDto> getCourses() {
        List<Course> courses = courseRepository.findAll();
        List<CourseDto> courseDtos =
                courses
                        .stream()
                        .map(course -> modelMapper
                                .map(course, CourseDto.class))
                        .collect(Collectors.toList());
        return null;
    }

    @Override
    public CourseDto getCourse(Long id) {
       Optional<Course> course = courseRepository.findById(id);
       if(course.isPresent()){
           return modelMapper.map(course.get(),CourseDto.class);

       }return null;
    }

    @Override
    public CourseDto updateCourse(Long id, CourseDto dto) {
        Course course = courseRepository.findById(id).orElseThrow(
                () -> new RuntimeException("dto with id:" + id + "  doesn't exist"));

//        Course map = modelMapper.map(dto, Course.class);

        Academy academy = academyRepository.findById(dto.getAcademy_id()).orElseThrow(
                ()-> new RuntimeException("Academy Id is Null or Not Found"));
        log.info("Update Course-> Academy {}",academy);
        course.setAcademy(academy);

        Teacher teacher = teacherRepository.findById(dto.getTeacher_id()).orElseThrow(
                ()-> new RuntimeException("Teacher Id is Null or Not Found"));
        log.info("Update Course-> Teacher {}",teacher);
        course.setTeacher(teacher);

        course.setName(dto.getName());
        course.setEnd_date(dto.getEnd_date());
        course.setStart_date(dto.getStart_date());

        Course savedCourse = courseRepository.save(course);
        log.info("Update Course-> Course Save {}",savedCourse);

        return modelMapper.map(savedCourse, CourseDto.class);
    }

    @Override
    public Boolean deleteCourse(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if (course.isPresent()){
            courseRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<CourseDto> findCourseByTeacherName(String name) {
        List<Course> courseByTeacherName = courseRepository.findCourseByTeacherName(name);
        List<CourseDto> list = new ArrayList<>();
        for (Course item : courseByTeacherName) {
            list.add(modelMapper.map(item,CourseDto.class));
        }

        return list;
    }


}
