package com.academy.AcademyApp.service.implementations;

import com.academy.AcademyApp.dto.AcademyDto;
import com.academy.AcademyApp.entity.Academy;
import com.academy.AcademyApp.repository.AcademyRepository;
import com.academy.AcademyApp.service.AcademyService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AcademyServiceImp implements AcademyService {
    final
    AcademyRepository academyRepository;
    final
    ModelMapper modelMapper;

    @Override
    public AcademyDto addAcademy(AcademyDto academyDto) {
        Academy academy = modelMapper.map(academyDto, Academy.class);

        return modelMapper.map(academyRepository.save(academy),AcademyDto.class);
    }

    @Override
    public List<AcademyDto> getAcademies() {
        List<Academy> academies = academyRepository.findAll();
        List<AcademyDto> academyDtos =
                academies
                        .stream()
                        .map(academy -> modelMapper
                        .map(academy, AcademyDto.class))
                        .collect(Collectors.toList());
       return null;
    }

    @Override
    public AcademyDto getAcademy(Long id) {
//        Optional<Academy> academy = academyRepository.findById(id);
//        if (academy.isPresent()){
//            return modelMapper.map(academy.get(),AcademyDto.class);
//        }
//        return null;
       Academy academy = academyRepository.findById(id).orElseThrow(()->new RuntimeException("id not found"));
       return modelMapper.map(academy,AcademyDto.class);

    }

    @Override
    public AcademyDto updateAcademy(Long id, AcademyDto academy) {
        academyRepository.findById(id).orElseThrow(() -> new RuntimeException("student with id:" + id +"  doesn't exist"));
        Academy map = modelMapper.map(academy, Academy.class);
        map.setId(id);
        return modelMapper.map(academyRepository.save(map), AcademyDto.class);
    }

    @Override
    public Boolean deleteAcademy(Long id) {
        Optional<Academy> academy = academyRepository.findById(id);
        if(academy.isPresent()){
            academyRepository.deleteById(id);
            return true;
        }return false;
    }
}
