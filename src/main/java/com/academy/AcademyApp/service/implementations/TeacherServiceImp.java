package com.academy.AcademyApp.service.implementations;

import com.academy.AcademyApp.dto.AcademyDto;
import com.academy.AcademyApp.dto.StudentDto;
import com.academy.AcademyApp.dto.TeacherDto;
import com.academy.AcademyApp.entity.Academy;
import com.academy.AcademyApp.entity.Course;
import com.academy.AcademyApp.entity.Student;
import com.academy.AcademyApp.entity.Teacher;
import com.academy.AcademyApp.repository.TeacherRepository;
import com.academy.AcademyApp.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TeacherServiceImp implements TeacherService {
    final
    ModelMapper modelMapper;
    private final TeacherRepository teacherRepository;


    @Override
    public TeacherDto addTeacher(TeacherDto teacherDto) {
        Teacher teacher = modelMapper.map(teacherDto, Teacher.class);

        Teacher savedTeacher = teacherRepository.save(teacher);
        TeacherDto map = modelMapper.map(savedTeacher, TeacherDto.class);
        return map;
    }

    @Override
    public List<TeacherDto> getTeachers() {
        List<Teacher> teachers = teacherRepository.findAll();
        List<TeacherDto> teacherDtos =
                teachers
                        .stream()
                        .map(teacher -> modelMapper
                                .map(teacher, TeacherDto.class))
                        .collect(Collectors.toList());
        return null;
    }

    @Override
    public TeacherDto getTeacher(Long id) {
        Teacher teacher = teacherRepository.findById(id).orElseThrow(()->new RuntimeException("id not found"));
        return modelMapper.map(teacher,TeacherDto.class);
    }

    @Override
    public TeacherDto updateTeacher(Long id, TeacherDto teacher) {
        teacherRepository.findById(id).orElseThrow(() -> new RuntimeException("Teacher with id:" + id +"  doesn't exist"));
        Teacher map = modelMapper.map(teacher, Teacher.class);
        map.setId(id);
        return modelMapper.map(teacherRepository.save(map), TeacherDto.class);
    }

    @Override
    public Boolean deleteTeacher(Long id) {
       Optional<Teacher> teacher = teacherRepository.findById(id);
       if (teacher.isPresent()){
           teacherRepository.deleteById(id);
           return true;
       }
       return false;
    }
}
