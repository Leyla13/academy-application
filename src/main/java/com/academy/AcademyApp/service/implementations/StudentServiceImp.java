package com.academy.AcademyApp.service.implementations;

import com.academy.AcademyApp.dto.StudentDto;
import com.academy.AcademyApp.entity.Course;
import com.academy.AcademyApp.entity.Student;
import com.academy.AcademyApp.repository.CourseRepository;
import com.academy.AcademyApp.repository.StudentRepository;
import com.academy.AcademyApp.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final ModelMapper modelMapper;


    @Override
    public String createStudent(StudentDto studentDto) {
        Student student = modelMapper.map(studentDto, Student.class);

        List<Long> courseIdList = studentDto.getCourseIdList();
        List<Course> courseList = new ArrayList<>();

        for (Long id : courseIdList) {
            courseList.add(courseRepository.findById(id).orElseThrow(()-> new RuntimeException("Course id not found")));
        }

        student.setCourseList(courseList);
        studentRepository.save(student);


        return "New student saved";

//        List<Optional<Course>> courses = new ArrayList<>();
//        studentDto.getCourseIdList().forEach(id-> courses.add(courseRepository.findById(id)));
//        student.setCourseList(courses);
//        return modelMapper.map(studentRepository.save(student), StudentDto.class);
    }
    @Override
    public List<StudentDto> getStudents() {
        List<Student> students = studentRepository.findAll();
        List<StudentDto> studentDtos = students.stream().map(student -> modelMapper.map(student, StudentDto.class)).collect(Collectors.toList());
        return studentDtos;
    }

    @Override
    public StudentDto getStudent(Long id) {
        Optional<Student> optStudent = studentRepository.findById(id);
        if (optStudent.isPresent()){
            return modelMapper.map(optStudent.get(),StudentDto.class);
        }
        else throw new RuntimeException("student with given id doesn't exist");
    }

    @Override
    public StudentDto updateStudent(Long id, StudentDto studentDto) {
//        Id-ye gore Studenti tapiriq yoxdursa exception throw edirik
        Student student = studentRepository.findById(id).orElseThrow(
                () -> new RuntimeException("student with id:" + id + "  doesn't exist"));
//        Student map = modelMapper.map(studentDto, Student.class);


//        Sonra tapdigimiz Entity-nin uzerine dto-dan aldigimiz datani set edirik
        student.setFirstName(studentDto.getFirstName());
        student.setLastName(studentDto.getLastName());
        student.setEmail(studentDto.getEmail());

//        Biz dto-dan Courselerin sadece Id-lerini aldigimiza gore o Id-lerin entitylerini tapiriq
//        ve Course Liste add edirik. Cunki Save etmek ucun Entity List<Course> qebul edir

        List<Course> list= new ArrayList<>();
        for (Long item : studentDto.getCourseIdList()) {
            list.add(courseRepository.getById(item));
        }
        student.setCourseList(list);
        return modelMapper.map(studentRepository.save(student), StudentDto.class);
    }

    @Override
    public Boolean deleteStudent(Long id) {
        Optional<Student> student = studentRepository.findById(id);
        if (student.isPresent()) {
            studentRepository.deleteById(id);
            return true;
        }
        return false;

    }

}
