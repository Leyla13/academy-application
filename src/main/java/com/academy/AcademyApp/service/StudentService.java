package com.academy.AcademyApp.service;

import com.academy.AcademyApp.dto.StudentDto;

import java.util.List;

public interface StudentService {
    String createStudent(StudentDto studentDto);
    List<StudentDto> getStudents();
    StudentDto getStudent(Long id);
    StudentDto updateStudent(Long id, StudentDto  studentDto);
    Boolean deleteStudent(Long id);

}
