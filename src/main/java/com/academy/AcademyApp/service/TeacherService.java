package com.academy.AcademyApp.service;

import com.academy.AcademyApp.dto.TeacherDto;
import com.academy.AcademyApp.entity.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherService {
    TeacherDto addTeacher(TeacherDto teacher);

    List<TeacherDto> getTeachers();
    TeacherDto getTeacher(Long id);
    TeacherDto updateTeacher(Long id, TeacherDto teacher);
    Boolean deleteTeacher(Long id);
}
