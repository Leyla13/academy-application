package com.academy.AcademyApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademyAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademyAppApplication.class, args);
	}

}
